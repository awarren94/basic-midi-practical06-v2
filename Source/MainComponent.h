/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage&) override;
    

private:
    AudioDeviceManager audioDeviceManager;
    Label midiLabel,
            channelLabel,
            numberLabel,
            velocityLabel,
            controlLabel;
    
    Slider channelSlider,
            numberSlider,
            velocitySlider,
            controlSlider;
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
